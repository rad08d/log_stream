# **Log Stream Processor**

This project is meant to process log message data in real time. Given a file of log data, it will read one message at a time and recompute statistics about all log data.

## **Getting Started**
To get started with this project:

1. clone repo
2. cd /project/root/
3. ./gradlew clean test build
4. java -jar ./build/libs/run_analysis.jar -f /path/to/log/message/file.txt

### **Prerequisites**

1. JDK 1.8

#### **Running the Tests**

1. cd ~/project/root
2. ./gradlew clean test

### **Author**
* Alan Dinneen Email: alan@implauzable.com Website: [implauzable](https://www.implauzable.com)

### **License**
This project is licensed under the Apache 2.0 License
