package com.implauzable.test.log

import com.implauzable.log.LogProcessor
import com.implauzable.log.LogMessage
import java.util.UUID
import io.kotlintest.specs.FunSpec
import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldEqual


class TestLogProcessor : FunSpec() {

    private val LOG_1 = LogMessage("/orchestra", 1514907673620L, UUID.fromString("6d707042-801a-41de-9406-54e46f6ae433"),"full-screen-ads", 4)
    private val LOG_2 = LogMessage("/guitar", 1514907682620L, UUID.fromString("6d707042-801a-41de-9406-54e46f6ae433"),"full-screen-ads", 4)
    private val LOG_3 = LogMessage("/bass-guitar", 1514907760620L, UUID.fromString("6d707042-801a-41de-9406-54e46f6ae433"),"full-screen-ads", 4)
    private val LOG_4 = LogMessage("/orchestra", 1514907673620L, UUID.fromString("6d803042-801a-41de-9406-54e46f6ae422"),"full-screen-ads", 5)
    private val LOG_5 = LogMessage("/keys", 1514907676620L, UUID.fromString("6d803042-801a-41de-9406-54e46f6ae422"),"full-screen-ads", 5)
    private val LOG_6 = LogMessage("/guitar", 1514907679620L, UUID.fromString("6d803042-801a-41de-9406-54e46f6ae419"),"full-screen-ads", 6)
    private val LOG_7 = LogMessage("/key-tar", 1514907732620L, UUID.fromString("6d803042-801a-41de-9406-54e46f6ae419"),"full-screen-ads", 6)
    private val LOG_8 = LogMessage("/violin", 1514907679620L, UUID.fromString("9d803042-801a-41de-9406-54e46f6ae418"),"full-screen-ads", 6)
    private val LOG_9 = LogMessage("/drum", 1514907771620L, UUID.fromString("9d803042-801a-41de-9406-54e46f6ae418"),"full-screen-ads", 6)


    init {
        test("The session time and start time for the specific UUID 6d707042-801a-41de-9406-54e46f6ae433 should be set and updated correctly") {
            val processor = LogProcessor()
            processor.processMessage(LOG_1)
            processor.processMessage(LOG_2)
            processor.processMessage(LOG_3)
            val session = processor.sessionTimes[UUID.fromString("6d707042-801a-41de-9406-54e46f6ae433")]
            session?.time shouldBe 87000L
            session?.start shouldBe 1514907760620L
        }

        test("The url visits counter should have 2 urls with one visit each and one url with 2 visits."){
            val processor = LogProcessor()
            processor.processMessage(LOG_1)
            processor.processMessage(LOG_2)
            processor.processMessage(LOG_3)
            processor.processMessage(LOG_4)
            processor.urlVisits[LOG_1.url] shouldEqual 2L
            processor.urlVisits[LOG_2.url] shouldEqual 1L
            processor.urlVisits[LOG_3.url] shouldEqual 1L
        }

        test("The url visits counter should have urls ordered by the least visited to the most visited."){
            val processor = LogProcessor()
            processor.processMessage(LOG_1)
            processor.processMessage(LOG_4)
            processor.processMessage(LOG_4)
            processor.processMessage(LOG_5)
            processor.processMessage(LOG_5)
            processor.processMessage(LOG_7)
            val target = hashMapOf("/key-tar" to 1L, "/keys" to 2L, "/orchestra" to 3L).toSortedMap()
            processor.urlLeaderBoard(3) shouldEqual target
        }

        test("The max time should be 87000 milliseconds and the min time should 3000 milliseconds") {
            val processor = LogProcessor()
            processor.processMessage(LOG_1)
            processor.processMessage(LOG_2)
            processor.processMessage(LOG_3)
            processor.processMessage(LOG_4)
            processor.processMessage(LOG_5)
            processor.processMessage(LOG_6)
            processor.processMessage(LOG_7)
            processor.minSessionTime shouldBe 3000L
            processor.maxSessionTime shouldBe 87000L
        }

        test("The mean session time should be 87000 milliseconds.") {
            val processor = LogProcessor()
            processor.processMessage(LOG_1)
            processor.processMessage(LOG_2)
            processor.processMessage(LOG_3)
            processor.processMessage(LOG_4)
            processor.processMessage(LOG_5)
            processor.processMessage(LOG_6)
            processor.processMessage(LOG_7)
            val target = (87000 + 3000 + 53000) / 3
            processor.meanSessionTime shouldEqual target.toLong()
        }

        test("The median session time for an odd count of sessions should be 53000 milliseconds and 70000 for an even count.") {
            val processor = LogProcessor()
            processor.processMessage(LOG_1)
            processor.processMessage(LOG_2)
            processor.processMessage(LOG_3)
            processor.processMessage(LOG_4)
            processor.processMessage(LOG_5)
            processor.processMessage(LOG_6)
            processor.processMessage(LOG_7)

            processor.medianSessionTime shouldEqual 53000L
            processor.processMessage(LOG_8)
            processor.processMessage(LOG_9)

            processor.medianSessionTime shouldEqual 70000L
        }
    }

}