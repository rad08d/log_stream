package com.implauzable.test.log


import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.implauzable.log.ReaderWriter
import io.kotlintest.specs.FunSpec
import com.implauzable.log.Report
import io.kotlintest.matchers.shouldEqual


class TestWriter : FunSpec() {
    init {
        test("A report should be serialized to JSON correctly") {
            val writer = ReaderWriter()
            val mapper = jacksonObjectMapper()
            val json = """{"most_viewed_urls":["/orchestra","/pro-audio","/pedals","/bass-guitars","/marketplace"],"session_stats":{"min":1000,"median":24040337,"max":24655000,"mean":24040337}}"""
            val target = mapper.readValue<Report>(json)

            val sessionStats = hashMapOf("min" to 1000L, "max" to 24655000L, "mean" to 24040337L, "median" to 24040337L)
            //Preserve order of URLs going into write function
            val urlLeaders = hashMapOf("/orchestra" to 1282L, "/pro-audio" to 1267L, "/pedals" to 1264L, "/bass-guitars" to 1261L , "/marketplace" to 1239L).toList().sortedBy { (_, V) -> V }.reversed().toMap()
            val output = mapper.readValue<Report>(writer.writeReport(urlLeaders, sessionStats))

            output shouldEqual target
        }
    }
}