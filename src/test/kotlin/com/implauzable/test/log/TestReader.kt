package com.implauzable.test.log

import com.implauzable.log.LogMessage
import com.implauzable.log.ReaderWriter
import java.util.UUID
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import io.kotlintest.matchers.shouldEqual
import io.kotlintest.matchers.shouldThrow
import io.kotlintest.specs.FunSpec
import java.time.format.DateTimeParseException


class TestReader : FunSpec() {

    private val GOOD_LOG_MESSAGE = """{
       "url":"/orchestra",
       "timestamp":"2018-01-02T15:41:13.620579",
       "sessionId":"6d707042-801a-41de-9406-54e46f6ae433",
       "experiments":"full-screen-ads",
       "userId":4
}"""

    private val MALFORMED_LOG_MESSAGE = """{
       "url":,
       "timestamp":"2018-01-02T15:41:13.620579",
       "sessionId":"6d707042-801a-41de-9406-54e46f6ae433",
       "experiments":"full-screen-ads",
       "userId":4
}"""


    private val BAD_LOG_DATE = """{
       "url":"/orchestra",
       "timestamp":"2018-01-02 15:41:13.62",
       "sessionId":"6d707042-801a-41de-9406-54e46f6ae433",
       "experiments":"full-screen-ads",
       "userId":4
}"""

    private val BAD_LOG_SESSION = """{
       "url":"/orchestra",
       "timestamp":"2018-01-02 15:41:13.62",
       "sessionId":"5dfas",
       "experiments":"full-screen-ads",
       "userId":4
}"""

    private val BAD_LOG_USER_ID = """{
       "url":"/orchestra",
       "timestamp":"2018-01-02 15:41:13.62",
       "sessionId":"5dfas",
       "experiments":"full-screen-ads",
       "userId":"4"
}"""
    init {

        test("A good log message should be converted into a log message object."){
            val reader = ReaderWriter()
            val expectedMessage = LogMessage("/orchestra", 1514907673620, UUID.fromString("6d707042-801a-41de-9406-54e46f6ae433"), "full-screen-ads", 4)
            val processedMessage = reader.readMessage(GOOD_LOG_MESSAGE)
            processedMessage shouldEqual expectedMessage
        }

        test("A malformed message JSON should throw the appropriate exception") {
            shouldThrow<JsonParseException> {
                val reader = ReaderWriter()
                reader.readMessage(MALFORMED_LOG_MESSAGE)
            }
        }

        test("An incorrect session id should throw the appropriate exception") {
            shouldThrow<InvalidFormatException> {
                val reader = ReaderWriter()
                reader.readMessage(BAD_LOG_SESSION)
            }
        }

        test("An incorrect user id should throw the appropriate exception") {
            shouldThrow<InvalidFormatException> {
                val reader = ReaderWriter()
                reader.readMessage(BAD_LOG_USER_ID)
            }
        }

        test("An incorrect log date should throw the appropriate exception") {
            shouldThrow<DateTimeParseException> {
                val reader = ReaderWriter()
                reader.readMessage(BAD_LOG_DATE)
            }
        }
    }
}