package com.implauzable.log

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.UUID


class ReaderWriter {
    private val mapper = jacksonObjectMapper()

    /**
     * Read a single/simple JSON Log Message payload and process the time string into epoch milliseconds (UTC)
     *
     * @param: JSON string representation of a log message
     *
     * @return: Log message with time string converted to epoch milliseconds (UTC)
     */
    fun readMessage(json: String): LogMessage {
        val message = mapper.readValue<RawLogMessage>(json)
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
        val epoch = LocalDateTime.parse(message.timestamp, formatter).toInstant(ZoneOffset.UTC).toEpochMilli()
        return LogMessage(message.url, epoch, message.sessionId, message.experiments, message.userId)
    }

    fun writeReport(urlLeaderBoard: Map<String, Long>, sessionStats: Map<String, Long>): String {
        val urlLeaders = urlLeaderBoard.map { (url, _) -> url }.toList()
        val report = Report(urlLeaders, sessionStats)
        return mapper.writeValueAsString(report)
    }
}

data class Report(@JsonProperty("most_viewed_urls") val urlLeaderBoard: List<String>, @JsonProperty("session_stats") val sessionStats: Map<String, Long>)
private data class RawLogMessage(val url: String, val timestamp: String, val sessionId: UUID, val experiments: String, val userId: Int)