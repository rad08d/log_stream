package com.implauzable.log

import org.slf4j.LoggerFactory
import java.util.UUID



/**
 * The log processor class is used to process log messages and store statistics about them.
 */
class LogProcessor {

    private val log = LoggerFactory.getLogger(javaClass)

    var meanSessionTime: Long = Long.MIN_VALUE
        private set
    var medianSessionTime : Long = Long.MIN_VALUE
        private set
    var maxSessionTime : Long = Long.MIN_VALUE
        private set
    var minSessionTime : Long = Long.MAX_VALUE
        private set

    val urlVisits = HashMap<String, Long>()

    //TODO: If this app is meant to run in a streaming fashion, expired session IDs should be periodically purged to keep memory low.
    val sessionTimes = HashMap<UUID, Session>()

    /**
     * Process a single log message.
     *
     * <p>
     * This method is the main entry point to process a single log message.
     *
     * @param:  message  an instance of a log message to process
     */
    fun processMessage(message: LogMessage) {
        log.info("Processing log message with session ID ${message.sessionId}")
        processSessionTime(message.sessionId, message.timestamp)
        minSessionTime = if (sessionTimes[message.sessionId]!!.time < minSessionTime && sessionTimes[message.sessionId]!!.time != 0L) sessionTimes[message.sessionId]!!.time else minSessionTime
        maxSessionTime = if (sessionTimes[message.sessionId]!!.time > maxSessionTime) sessionTimes[message.sessionId]!!.time else maxSessionTime
        meanSessionTime =  if (sessionTimes[message.sessionId]!!.time != 0L) computeRunningMean(meanSessionTime, sessionTimes[message.sessionId]!!.time, sessionTimes.count()) else meanSessionTime
        medianSessionTime = computeRunningMedian(sessionTimes)
        processUrl(message.url)
        log.info("Successfully processed log message with session ID ${message.sessionId}")
    }

    /**
     * Method to return the top n number of visited URLs.
     *
     * @param: number   top number of visited URLs to return
     *
     * @return: map     the top number of visited URLs
     */
    fun urlLeaderBoard(number: Int): Map<String, Long> {
        return sortMapByValue(urlVisits).takeLast(number).reversed().toMap()
    }

    /**
     * Generic function to sort a map by values.
     *
     * @param: map      map to be sorted
     *
     * @return: list    list of key value pairs that have been sorted.
     */
    private fun <K, V : Comparable<V>> sortMapByValue(map: Map<K, V>): List<Pair<K,V>> {
        return map.toList().sortedBy { (_, V) -> V }
    }

    /**
     * Compute the current mean after adding another log message
     *
     * @param: currentMean  the current running mean
     * @param: newValue     the new value to be added to the mean
     * @param: total        the total amount of numbers making up the mean
     */
    private fun computeRunningMean(currentMean: Long, newValue: Long, total: Int): Long {
        return currentMean * (total - 1) / total + newValue / total
    }

    /**
     * Compute the current median of all session times.
     *
     * @param:  map     a collection of the sessions and their current running times
     *
     * @return: long    the median session time of users.
     */
    private fun computeRunningMedian(map: Map<UUID, Session>): Long {
        val sorted = sortMapByValue(map)
        return if (sorted.count() % 2 == 0) {
                    (sorted[sorted.count()/2].second.time + sorted[sorted.count()/2-1].second.time) / 2
                } else sorted[sorted.count() / 2 ].second.time
    }
    /**
     * Process the count of URLs visited.
     * <p>
     * This method takes a URL and adds it to the LogProcessor's URL visted counts.
     *
     * @param:  url  the url that was visited.
     */
    private fun processUrl(url: String) {
        when (urlVisits.containsKey(url)) {
            true -> urlVisits.put(url, urlVisits[url]!!.inc())
            false -> urlVisits.put(url, 1L)
        }
    }

    /**
     * Process a session's cumulative elapsed time.
     * <p>
     *  This method will take in a session id as a UUID and a epoch timestamp in milliseconds and add/update the
     *  session in the LogProcessor's session times table.
     *
     *  @param:  sessionId  the session's UUID
     *  @param:  timestamp  the session's most recent activity timestamp
     */
    private fun processSessionTime(sessionId: UUID, timestamp: Long) {
        when (sessionTimes.containsKey(sessionId)) {
            true -> sessionTimes.put(sessionId, updateSessionTime(sessionTimes[sessionId]!!, timestamp))
            false -> sessionTimes.put(sessionId, Session(sessionId, timestamp, 0))
        }
    }

    /**
     * Update an existing session to get the current elapsed time of a session.
     * <p>
     * This method uses a timestamp from a new session activity (eg. navigate to a new URL) to subtract from the
     * timestamp of the last session activity. The result is added to the existing session time to get the
     * elapsed time of the session.
     *
     * @param:  session     the existing session to be updated
     * @param:  timestamp   the current timestamp of the new activity that occurred during the session
     *
     * @return: session     a session object with the relevant data about the session
     */
    private fun updateSessionTime(session: Session, timestamp: Long): Session {
        session.time += timestamp - session.start
        session.start = timestamp
        return session
    }

}

data class Session(val sessionId: UUID, var start: Long, var time: Long): Comparable<Session> {
    override fun compareTo(other: Session): Int {
       return when {
            this.time == other.time -> 0
            this.time > other.time  -> 1
            else -> -1
        }
    }
}
data class LogMessage(val url: String, val timestamp: Long, val sessionId: UUID, val experiments: String, val userId: Int)
