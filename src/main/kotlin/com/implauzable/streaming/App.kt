package com.implauzable.streaming

import java.io.File
import java.io.InputStream
import com.implauzable.log.LogProcessor
import com.implauzable.log.ReaderWriter
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import org.slf4j.LoggerFactory
import java.io.FileNotFoundException

val log = LoggerFactory.getLogger("LogStreamProcessor")

fun main(args : Array<String>) = mainBody {

    val parsedArgs = ArgParser(args).parseInto(::ParsedArgs)
    val processor = LogProcessor()
    parsedArgs.run {

        try {
            // Read in file from command line args
            val inputStream: InputStream = File(this.filePath).inputStream()
            val readerWriter = ReaderWriter()
            inputStream.bufferedReader().useLines { lines -> lines.forEach {
                try {
                    processor.processMessage(readerWriter.readMessage(it))
                } catch(e: Exception) {
                    log.error("There has been an exception while processing a log message!", e)
                }

            } }

            // Report the statistics found
            val sessionStats = hashMapOf("median" to processor.meanSessionTime,
                    "mean" to processor.meanSessionTime,
                    "max" to processor.maxSessionTime,
                    "min" to processor.minSessionTime)
            log.info(readerWriter.writeReport(processor.urlLeaderBoard(5), sessionStats))
        } catch (fex: FileNotFoundException){
            log.error("The path provided is an invalid filepath.", fex)
        }

    }
}


class ParsedArgs(parser: ArgParser) {
    val filePath by parser.storing("-f", "--filePath",
            help = "The full path to the file to process.")
}
